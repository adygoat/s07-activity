Adrian M. Monteclaro BSIT -3 

C:\Users\ady>mysql -u root
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 5.5.5-10.4.24-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| bankapp            |
| blog_db            |
| createevent        |
| dbeventsystem      |
| information_schema |
| jeepcodes          |
| music_db           |
| mysql              |
| performance_schema |
| phpapp2            |
| phpmyadmin         |
| quiz               |
| quizevent          |
| restaurant         |
| test               |
| testlang           |
+--------------------+
16 rows in set (0.08 sec)

mysql> USE blog_db;
Database changed
mysql> INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA", "2021-01-01 01:00:00");
Query OK, 1 row affected (0.18 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("juandelecruz@gmail.com","passwordB", "2021-01-01 02:00:00");
Query OK, 1 row affected (0.04 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC", "2021-01-01 03:00:00");
Query OK, 1 row affected (0.02 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD", "2021-01-01 04:00:00");
Query OK, 1 row affected (0.05 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordE", "2021-01-01 05:00:00");
Query OK, 1 row affected (0.02 sec)

mysql> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.00 sec)

mysql> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelecruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+
5 rows in set (0.01 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World", "2021-01-02 01:00:00");
Query OK, 1 row affected (0.05 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth", "2021-01-02 02:00:00");
Query OK, 1 row affected (0.03 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
Query OK, 1 row affected (0.13 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye Solar System!", "2021-01-02 04:00:00");
Query OK, 1 row affected (0.09 sec)

mysql> SELECT * FROM posts;
+----+-----------+-----------------------+---------------------+-------------+
| id | author_id | content               | datetime_posted     | title       |
+----+-----------+-----------------------+---------------------+-------------+
|  1 |         1 | Hello World           | 2021-01-02 01:00:00 | First Code  |
|  2 |         1 | Hello Earth           | 2021-01-02 02:00:00 | Second Code |
|  3 |         2 | Welcome to Mars!      | 2021-01-02 03:00:00 | Third Code  |
|  4 |         4 | Bye bye Solar System! | 2021-01-02 04:00:00 | Fourth Code |
+----+-----------+-----------------------+---------------------+-------------+
4 rows in set (0.00 sec)

mysql> SELECT * FROM posts WHERE author_id = 1;
+----+-----------+-------------+---------------------+-------------+
| id | author_id | content     | datetime_posted     | title       |
+----+-----------+-------------+---------------------+-------------+
|  1 |         1 | Hello World | 2021-01-02 01:00:00 | First Code  |
|  2 |         1 | Hello Earth | 2021-01-02 02:00:00 | Second Code |
+----+-----------+-------------+---------------------+-------------+
2 rows in set (0.03 sec)

mysql> SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelecruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+
5 rows in set (0.00 sec)

mysql> UPDATE posts SET content = "Hello to the People of the Earth!" WHERE id = 2;
Query OK, 1 row affected (0.11 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM posts;
+----+-----------+-----------------------------------+---------------------+-------------+
| id | author_id | content                           | datetime_posted     | title       |
+----+-----------+-----------------------------------+---------------------+-------------+
|  1 |         1 | Hello World                       | 2021-01-02 01:00:00 | First Code  |
|  2 |         1 | Hello to the People of the Earth! | 2021-01-02 02:00:00 | Second Code |
|  3 |         2 | Welcome to Mars!                  | 2021-01-02 03:00:00 | Third Code  |
|  4 |         4 | Bye bye Solar System!             | 2021-01-02 04:00:00 | Fourth Code |
+----+-----------+-----------------------------------+---------------------+-------------+
4 rows in set (0.00 sec)

mysql> DELETE FROM users WHERE email = "johndoe@gmail.com";
Query OK, 1 row affected (0.06 sec)

mysql> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelecruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+
4 rows in set (0.00 sec)